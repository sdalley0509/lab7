/*    Spencer Dalley
      CPSC 1021, Section 3, F20
      sdalley@clemson.edu
      Elliot McMillan
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
    string lastName;
    string firstName;
    int birthYear;
    double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) {return rand()%i;}

int main(int argc, char const *argv[]) {

  srand(unsigned (time(0))); //Seed random generator

  employee arr[10];
  employee *ptr, *endPtr;
  ptr = &arr[0];//Pointer that points to first element in array
  endPtr = &arr[10]; //Pointer that points to last element in array

  int i;

  for (i = 0; i < 10; i++) { //For loop that fills employee array
    cout << "Enter last name: " << endl;
    cin >> arr[i].lastName;
    cout << "Enter first name: " << endl;
    cin >> arr[i].firstName;
    cout << "Enter birth year: " << endl;
    cin >> arr[i].birthYear;
    cout << "Enter hourly wage: " << endl;
    cin >> arr[i].hourlyWage;
  }

  random_shuffle(ptr, endPtr, myrandom); //Calls random_shuffle function

  employee newarr[5] = {arr[0], arr[1], arr[2], arr[3], arr[4]};

  sort(&newarr[0], &newarr[5], name_order); //Calls sort function

  for (i = 0; i < 5; i++) { //For loop to print the sorted employees
    cout << newarr[i].lastName << ", " << newarr[i].firstName << endl << endl;
    cout << newarr[i].birthYear << endl << endl;
    cout << newarr[i].hourlyWage << endl << endl;
  }



  return 0;
}

bool name_order(const employee& lhs, const employee& rhs)
{return lhs.lastName < rhs.lastName;}
